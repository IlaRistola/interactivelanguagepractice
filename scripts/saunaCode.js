 //
    ///// RADICAL RABBIT /////
    // hello@radicalrabbit.fi //
   
    // check which mission scene is active to choose right videos and wordlists
    var url = location.pathname;
    var checkScene = url.substring(url.lastIndexOf("/"));
    
    if(checkScene === "/mission1.html"){
    var json = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "wordlists/words1.json",
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();
    // videolist with totally 12 videos 0-11 // //Videolist should be under json, when we add those to amazon s3 bucket
    var videoList = ["video/mis1/m1_intro.mp4","video/mis1/m1_loop.mp4","video/mis1/m1_a1.mp4","video/mis1/m1_a2.mp4","video/mis1/m1_c1.mp4","video/mis1/m1_b1.mp4","video/mis1/m1_d1.mp4","video/mis1/m1_h1.mp4","video/mis1/m1_fail.mp4","video/mis1/m1_example.mp4","video/mis1/m1_h2.mp4"];
  } else if(checkScene === "/mission2.html"){
    var json = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "wordlists/words2.json",
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();
    var videoList = ["video/mis2/m2_intro.mp4","video/mis2/m2_loop.mp4","video/mis2/m2_a1.mp4","video/mis2/m2_a2.mp4","video/mis2/m2_c1.mp4","video/mis2/m2_b1.mp4","video/mis2/m2_d3.mp4","video/mis2/m2_h1.mp4","video/mis2/m2_fail.mp4","video/mis2/m2_example.mp4","video/mis2/m2_h2.mp4"];
    } else if(checkScene === "/mission3.html"){                                 
    var json = (function() {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "wordlists/words3.json",
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();
    var videoList = ["video/mis3/m3_intro.mp4","video/mis3/m3_loop.mp4","video/mis3/m3_a1.mp4","video/mis3/m3_a2.mp4","video/mis3/m3_c1.mp4","video/mis3/m3_b1.mp4","video/mis3/m3_d2.mp4","video/mis3/m3_h1.mp4","video/mis3/m3_fail.mp4","video/mis3/m3_example.mp4","video/mis3/m3_h2.mp4"];
    }

  var sec = 0;
  var timing = 0;
  var wordList = json;
  var allWords = wordList.kirosanat + wordList.oikeaVastaus + wordList.melkeinOikein + wordList.muitaSanoja;
  var rightWords = wordList.oikeaVastaus;
  var almostWords = wordList.melkeinOikein;
  var badWords = wordList.kirosanat;
  var wrongNumbers = wordList.muitaSanoja;
  var vid = document.getElementById("vidPlayer");
  var vid2 = document.getElementById("vidPlayer2");
  var pic = document.getElementById("mPic");
  fButton = document.getElementById("nappi3");
  var textBox = document.getElementById("speechbox");
  $('#speechbox').hide();
  var recButton = document.getElementById("nappi2");
  var infoButton = document.getElementById("nappi1");
  infoButton.disabled = true;
  recButton.disabled = true;
  vid.setAttribute("src", videoList[0]);
  vid2.setAttribute("src", videoList[1]); //
  var tryOuts = 0;
  
  function touch_start(e){
  if(!recButton.disabled){
  e.preventDefault();
  e.target.onclick();
  }
}
    
  // speechRecognition system  
    window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition;
    let finalTranscript = '';
    let recognition = new window.SpeechRecognition();
    recognition.lang = 'fi-FI';
    recognition.interimResults = true;
    recognition.maxAlternatives = 10;
    recognition.continuous = false;
    //Start listening when user touchDown or tap mic button
    function speechListener() {
    finalTranscript = '';
    recognition.onresult = (event) => {
      let interimTranscript = '';
      for (let i = event.resultIndex, len = event.results.length; i < len; i++) {
        let transcript = event.results[i][0].transcript;
        if (event.results[i].isFinal) {
          finalTranscript += transcript;
        } else {
          interimTranscript += transcript;
        }
      }
    };
    recognition.start();
    startCount();
    setTimeout(function(){
        //lets ad manual stop just in case
        recognition.stop();
    }, 10000);
    recButton.disabled = true;
    infoButton.disabled = true;
    recButton.style.backgroundImage = 'url(graphic/micRunning.png)';
    infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
    }
        recognition.onend = function() {
          stopCount();
          textBox.innerHTML = "" + finalTranscript;
          recButton.disabled = true;
          recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
          setTimeout(function(){ findWords();    //there is timeout, so find recognition has same save time to create result
    }, 1000);
};

    function goForward(){
      pic.style.display = "none";
      vid.style.display = "block";
      fButton.disabled = false;
      fButton.style.backgroundImage = 'url(graphic/arrowDefault.png)';
      fButton.setAttribute("onclick", "false");
      vid.play();
    }

    function startCount() {
      // counter for user to mic recording time. Note: System still autostop if its get speech and break
      timing = setInterval(function(){ 
        sec += 1;
        textBox.innerHTML = "00:0"+sec;
        if(sec == 10){
          stopCount();
        }
       }, 1000);
  }
  
  function stopCount() {
      sec = 0;
     clearInterval(timing);
  }

    function missionGuide(){
      vid.setAttribute("src", videoList[9]);
      vid.load();
      vid.setAttribute("onended", "vidEnd()");
      vid.setAttribute("onclick", "vidEnd()");
      recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
      recButton.disabled = true;
      infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
      infoButton.disabled = true;
      $('#speechbox').hide();
    }

    function vidEnd() {
      textBox.innerHTML = "";
      vid2.play();
      vid2.setAttribute("loop", true);
      vid2.style.display = "block";
      vid.style.display = "none";
      vid.setAttribute("onended", "false");
      recButton.disabled = false;
      infoButton.disabled = false;
      $('#speechbox').show();
      recButton.style.backgroundImage = 'url(graphic/micActive.png)';
      infoButton.style.backgroundImage = 'url(graphic/helpActive.png)';
      if(vid.hasAttribute("onclick", "vidEnd")){
        vid.setAttribute("onclick", "false");
        vid.pause();
      }
  }

  vid.onloadeddata = function() {
    if(pic.style.display == "block"){
      fButton.style.backgroundImage = 'url(graphic/arrowActive.png)';
      fButton.disabled = false;
    } else{
    // we use two videoplayer's so experience look more seamless.
    vid.play();
    vid2.pause();
    vid.style.display = "block";
    vid2.style.display = "none";
    }
    }

     function missionEnd(){
      if(checkScene === "/mission1.html"){
        window.location.href="mission2.html";
      } else if(checkScene === "/mission2.html"){
        window.location.href="mission3.html";
      } else {
        window.location.href="endScreen.html";
      }
     }

     // Trs's is transition videos and functions after we get answer from user.
     function Trs(){
      vid.setAttribute("src", videoList[2]);
      vid.load();
      vid.setAttribute("onended", "missionEnd()");
      
     }

     function Trs1(){
      vid.setAttribute("src", videoList[3]);
      vid.load();
      vid.setAttribute("onended", "missionEnd()");
     }

     function Trs4(){
      if(tryOuts < 2){
        vid.setAttribute("src", videoList[4]);
        vid.load();
        vid.setAttribute("onended", "vidEnd()");
      } else if(tryOuts == 2){
        vid.setAttribute("src", videoList[4]);
      vid.load();
        vid.setAttribute("onended", "Helpme()");
      } else if(tryOuts == 3){
        vid.setAttribute("src", videoList[4]);
      vid.load();
        vid.setAttribute("onended", "HelpmeTwice()");
      } else{
        missionFail();
      }
     }

     function Trs5(){
      if(tryOuts < 2){
        vid.setAttribute("src", videoList[5]);
        vid.load();
        vid.setAttribute("onended", "vidEnd()");
      } else if(tryOuts == 2){
        Helpme();
      } else if(tryOuts == 3){
        HelpmeTwice();
      } else{
        missionFail();
      }
     }
     
     function Trs6(){
      if (tryOuts < 2){
        vid.setAttribute("src", videoList[6]);
        vid.load();
        if(checkScene === "/mission3.html"){
        vid.setAttribute("onended", "missionEnd()");
        } else{
        vid.setAttribute("onended", "vidEnd()");
        }
      } else if(tryOuts == 2){
        Helpme();
      } else if(tryOuts == 3){
        HelpmeTwice();
      } else{
        missionFail();
      }
     }

     function Helpme(){
      vid.setAttribute("src", videoList[7]);
      vid.load();
      vid.setAttribute("onended", "vidEnd()");
     }

     function HelpmeTwice(){
      vid.setAttribute("src", videoList[10]);
      vid.load();
      vid.setAttribute("onended", "vidEnd()");
     }

     function missionFail(){
      vid.setAttribute("src", videoList[8]);
      vid.load();
      vid.setAttribute("onended", "missionEnd()");
     }

    // start seeking right words after listening is stopped
    function findWords(){
        console.log(tryOuts);
        var checkText = finalTranscript.toLowerCase();
		// We use booleans to control answers which found from array after compared it to finalTranscript
        var correctAnswer = false; 
        var almostAnswer = false;
        var badAnswer = false;
		    var wrongNum = false;
       
       for (i = 0; i < allWords.length; i++){
         if(checkText.includes(badWords[i])){
          badAnswer = true; 
           break;
         } else if(checkText.includes(rightWords[i])){
          correctAnswer = true;
         } else if (checkText.includes(almostWords[i])){
        almostAnswer = true;
        } else if (checkText.includes(wrongNumbers[i])){
        wrongNum = true;
        }
       }   
      
        // check which boolean is active. badAnswer is first to looking, because we want give feed back, if user give any bad languages
        setTimeout(function(){
       if(badAnswer){
          tryOuts++;
          Trs4();
        }
        else if(correctAnswer){
            Trs();
        } else if(almostAnswer) {
            Trs1(); 
        }else if(wrongNum) {
            tryOuts++;
            Trs6(); 
        } else{
            tryOuts++;
            Trs5();
        }
        }, 1000);
    }
    
    // created by Ila Ristola //
                                  
    //  //      //               
   //  //     /////            
  //  /////  //  //  