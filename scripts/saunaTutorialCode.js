     ///// RADICAL RABBIT /////
    // hello@radicalrabbit.fi //
   
  //json wordlist for tutorial
  var json = (function() {
    var json = null;
    $.ajax({
            'async': false,
            'global': false,
            'url': "wordlists/tutorialWords.json",
            'dataType': "json",
            'success': function (data) {
                json = data;
            }
        });
    return json;
    })();

  var wordList = json;
  var allWords = wordList.oikeaVastaus;
  var tutorialWords = wordList.oikeaVastaus;

var sec = 0;
var timing = 0;


  var vid = document.getElementById("vidPlayer");
  var img = document.getElementById("tutorialImages");
  // 0-2 videos
  var videoList = ["video/tutorial/t_mic.mp4","video/tutorial/t_help.mp4","video/tutorial/Opener.mp4"];
  // 0-5 images
  var imageList = ["graphic/tutorialStart.png","graphic/MicRight.png","graphic/NoiseRight.png","graphic/NotUnderstand.png","graphic/InfoInput.png","graphic/logo_alku.png"];
  var correct = document.getElementById("Correct");
  var noiseCorrect = document.getElementById("noiseCorrect");
  var textBox = document.getElementById("speechbox");
  var a = 0;
  $('#speechbox').hide();
  var recButton = document.getElementById("nappi2");
  var infoButton = document.getElementById("nappi1");
  var forwardButton = document.getElementById("nappi3");
  var pointL = document.getElementById("pointsLevel");
  var noiseRec;

  recButton.style.opacity = 0;
  recButton.disabled = true;
  forwardButton.style.backgroundImage = 'url(graphic/harjoittele.png)';
  infoButton.style.backgroundImage = 'url(graphic/aloita.png)';
  forwardButton.disabled = true;
  infoButton.disabled = true;
  vid.setAttribute("src", videoList[0]);
  vid.load();
  setTimeout(function(){
    //for handle instant click
  forwardButton.disabled = false;
  infoButton.disabled = false;
  }, 1000);

  function startCount() {
    timing = setInterval(function(){ 
      sec += 1;
      textBox.innerHTML = "00:0"+sec;
      if(sec == 10){
        stopCount();
      }
     }, 1000);
}

function stopCount() {
    sec = 0;
   clearInterval(timing);
}

  function startTraining(){
  infoButton.style.opacity = 0;
  pointL.style.opacity = 1;
  forwardButton.setAttribute("onclick", "micTraining()");
  forwardButton.style.backgroundImage = 'url(graphic/arrowActive.png)';
  infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
  recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
  infoButton.disabled = true;
  infoButton.setAttribute("onclick", "missionGuide()");
  vid.setAttribute("src", videoList[0]);
  vid.style.display = "block";
  img.style.display = "none";
  vid.play();
  vid.setAttribute("loop", true);
  }

  function micTraining(){
  infoButton.style.opacity = 0;
  pointL.style.opacity = 1;
  forwardButton.style.backgroundImage = 'url(graphic/arrowDefault.png)';
  infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
  recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
  infoButton.disabled = true;
  infoButton.setAttribute("onclick", "missionGuide()");
  forwardButton.disabled = true;
  recButton.style.opacity = 1;
  img.style.display = "none";
  vid.style.display = "block";
  vid.play();
  vid.setAttribute("loop", true);
  setTimeout(function(){
    recButton.disabled = false;
    recButton.style.backgroundImage = 'url(graphic/micActive.png)';
    $('#speechbox').show();
  }, 2000);
  }
  
  function touch_start(e){
  if(!recButton.disabled){
  e.preventDefault();
  e.target.onclick();
  }
}
    
  // speechRecognition system  
    window.SpeechRecognition = window.webkitSpeechRecognition || window.SpeechRecognition;
    let finalTranscript = '';
    let recognition = new window.SpeechRecognition();
    recognition.lang = 'fi-FI';   // That can be give better result?
    recognition.interimResults = true;
    recognition.maxAlternatives = 10;
    recognition.continuous = false;
    //Start listening when user touchDown mic button
    function speechListener() {
    finalTranscript = '';
    recognition.onresult = (event) => {
      let interimTranscript = '';
      for (let i = event.resultIndex, len = event.results.length; i < len; i++) {
        let transcript = event.results[i][0].transcript;
        if (event.results[i].isFinal) {
          finalTranscript += transcript;
        } else {
          interimTranscript += transcript;
        }
      }
    };
    recognition.start();
    startCount();
    noiseRec = setTimeout(function(){
        a = 1;
        recognition.stop();
    }, 10000);
    recButton.disabled = true;
    infoButton.disabled = true;
    recButton.style.backgroundImage = 'url(graphic/micRunning.png)';
    infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
    }
        recognition.onend = function() {
          stopCount();
          textBox.innerHTML = "" + finalTranscript;
          clearTimeout(noiseRec);
          recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
          setTimeout(function(){ findWords();    //there is timeout, so find recognition has same save time to create result
    }, 1000);
};

    // start seeking right words after listening is stopped
    function findWords(){
      var checkText = finalTranscript.toLowerCase();
      var correctAnswer = false; 

      for (i = 0; i < allWords.length; i++){
      if(checkText.includes(tutorialWords[i])){
        correctAnswer = true;
            break;
    }
    }

    setTimeout(function(){
      if(correctAnswer && a == 1){
        noiseRight();
      } else if(correctAnswer){
        GoodOne();
       } else{
          notUnderstand();
       }
       }, 1000);

  }

    function GoodOne(){
     img.setAttribute("src", imageList[1]);
     vid.style.display = "none";
     img.style.display = "block";
     vid.setAttribute("src", videoList[1]);
     vid.load();
     setTimeout(function(){
       helpUsing();
     }, 4000);
    }

    function noiseRight(){
      img.setAttribute("src", imageList[2]);
      vid.style.display = "none";
      img.style.display = "block";
      vid.setAttribute("src", videoList[1]);
      vid.load();
     setTimeout(function(){
      helpUsing();
    }, 4000);
    }

    function notUnderstand(){
      img.setAttribute("src", imageList[3]);
      vid.style.display = "none";
      img.style.display = "block";
      a = 0;
      setTimeout(function(){
        recButton.disabled = false;
        recButton.style.backgroundImage = 'url(graphic/micActive.png)';
        img.style.display = "none";
        vid.style.display = "block";
      }, 4000);
    }

    function helpUsing(){
      forwardButton.disabled = true;
      forwardButton.style.backgroundImage = 'url(graphic/arrowDefault.png)';
      infoButton.style.opacity = 1;
      pointL.setAttribute('src', 'graphic/2point.png');
      infoButton.disabled = false;
      infoButton.style.backgroundImage = 'url(graphic/helpActive.png)';
      $('#speechbox').hide();
      vid.play();
      img.style.display = "none";
      vid.style.display = "block";
      img.setAttribute("src", imageList[4]);
    }
    var afterHelp;

    function missionGuide(){
      infoButton.disabled = true;
      infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
      img.style.display = "block";
      vid.style.display = "none";
      vid.pause();
      forwardButton.setAttribute("onclick", "openerVideo()");
      img.setAttribute("onclick", "startExperience()");
      afterHelp = setTimeout(function(){ 
       startExperience();
      }, 6000);
    }

    function startExperience(){
      clearTimeout(afterHelp);
      img.setAttribute("onclick", "false");
      pointL.setAttribute('src', 'graphic/3point.png');
      img.setAttribute("src", imageList[5]);
      infoButton.disabled = true;
      infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
      vid.setAttribute("src", videoList[2]);
      vid.loop = false;
      vid.setAttribute("onended", "startMission()");
      vid.onloadeddata = function() {
      forwardButton.disabled = false;
      forwardButton.style.backgroundImage = 'url(graphic/arrowActive.png)';
      }
    }

    function openerVideo(){
      pointL.style.opacity= 0;
      forwardButton.style.backgroundImage = 'url(graphic/arrowDefault.png)';
      forwardButton.disabled = true;
      vid.style.display = "block";
      img.style.display = "none";
      vid.play();
    }

    function skipTraining(){
      vid.setAttribute("src", videoList[2]);
      recButton.style.opacity = 1;
      forwardButton.style.backgroundImage = 'url(graphic/arrowDefault.png)';
      infoButton.style.backgroundImage = 'url(graphic/helpDefault.png)';
      recButton.style.backgroundImage = 'url(graphic/micDefault.png)';
      forwardButton.disabled = true;
      infoButton.disabled = true;
      recButton.disabled = true;
      vid.onloadeddata = function() {
        vid.play();
        img.style.display = "none";
        vid.style.display = "block";
        vid.setAttribute("onended", "startMission()");
        }
    }

    function startMission(){
      window.location.href="mission1.html";
    }

    // created by Ila Ristola //

    
    //  //      //
   //  //     /////
  //  /////  //  //