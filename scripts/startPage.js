    ///// RADICAL RABBIT /////
    // hello@radicalrabbit.fi //

var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
var forwardButton = document.getElementById("nappi3");
var tBox = document.getElementById("textField");
var tBox2 = document.getElementById("textField2");
var img = document.getElementById("startImages");
// images 0-2
var imageList = ["graphic/logo_apple.png","graphic/logo_mic.png","graphic/logo_noMic.png"];

forwardButton.style.backgroundImage = 'url(graphic/arrowActive.png)';

/*we had to create a device type checker because the demo version of speech recognition does not support iOS mobile devices*/
if(iOS == true){
  img.setAttribute("src", imageList[0]);
  tBox.style.display = "none";
  tBox2.style.display = "block";
  forwardButton.disabled = true;
  forwardButton.style.opacity = 0;
}


function secondStartScreen(){
    img.setAttribute("src", imageList[1]);
    tBox.innerHTML = "Hyväksy mikrofonin käyttäminen jatkaaksesi kokemusta. Allow access to device's microphone to proceed.";
    forwardButton.disabled = true;
    forwardButton.setAttribute("onclick", "allowMic()");
    setTimeout(function(){
      //for handle instant click
forwardButton.disabled = false;
}, 1000);
}

/* Check that the user accepts the use of the microphone. If user try to continue without using the mic, we will instruct to activate the mic */
function allowMic(){
  navigator.mediaDevices.getUserMedia({ audio: true })
  .then(function(stream) {
    console.log('You let me use your mic!');
    window.location.href="tutorial.html";
  })
  .catch(function(err) {
    img.setAttribute("src", imageList[2]);
    forwardButton.disabled = true;
    forwardButton.style.opacity = 0;
    tBox.style.display = "none";
    tBox2.style.display = "block";
    tBox2.setAttribute('href','https://support.google.com/chrome/answer/2392709?co=GENIE.Platform%3DAndroid&hl=fi');
    tBox2.innerHTML = "Tyhjennä selaimen sivustoasetukset jatkaaksesi";
  });
} 

// created by Ila Ristola //

    //  //      //               
   //  //     /////            
  //  /////  //  //